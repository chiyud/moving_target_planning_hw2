# -*- coding: utf-8 -*-
"""
Created on Fri Oct 14 15:28:56 2016

auxiliaries

@author: chiyud
"""

import pickle
import numpy as np

from heapq import heappush, heappop
import itertools

class loaddata():
    def __init__(self,datapath = 'problems/problem_0.txt'):
        self.config = {}
        self.config['dim'] = []
        self.config['initial'] = []
        self.config['target'] = []
        self.config['costs'] = []
        flag = 'dim';
        with open(datapath,'r') as f:
                        
            for line in f:
                if  line[0] == 'N':
                    flag = 'dim'
                    
                elif line[0] == 'R':
                    flag = 'initial'
                    
                elif line[0] == 'T':
                    flag = 'target'
                    
                elif line[0] == 'B':
                    flag = 'costs'
                
                else:
                    if line != '\n':
                        self.config[flag].append(self.toNum(line))
        self.config['dim'] = self.config['dim'][0]
        
    def toNum(self, line):
        result = []
        line = line.split(',')
        for c in line:
            result.append(int(c))
                
        return result
        
    def checkBoundary(self, pose):
        y = pose[0]
        x = pose[1]
        if y < 0 or y >= self.config['dim'][0] or\
                    x < 0  or x>=self.config['dim'][0]:
            return False
        else:
            return pose
        
    def checkCatch(self, Q, step):
        T = self.config['target'][step]  
        MIN = INT_MAX
        for node in Q:
            if node.pose == T:
                if node.total_cost < MIN:
                    MIN = node.total_cost
        
    def getCost(self, loc):
        return self.config['costs'][loc[0]][loc[1]]
    
                    

def toString(l):
    s = ', '.join(str(x) for x in l)
    return s

def toList(l):
    s = l.split(',')
    return [int(i) for i in s]



class priorityQ:# This class is modified from Python.org
    def __init__(self):
        
        self.pq = []                         # list of entries arranged in a heap
        self.entry_finder = {}               # mapping of tasks to entries
        self.REMOVED = '<removed-task>'      # placeholder for a removed task
        self.counter = itertools.count()     # unique sequence count

    def add_task(self, task, pre_g=0, c = 0, h = 0):
    #'Add a new task or update the priority of an existing task'
        priority = pre_g + c + h
        g = pre_g + c
        if task in self.entry_finder:
            if self.entry_finder[task][1] > g:
                self.remove_task(task)
            else:
                return False
            
        count = next(self.counter)
        entry = [priority, g, count, task]
        self.entry_finder[task] = entry
        heappush(self.pq, entry)
        return task

    def remove_task(self,task):
    #'Mark an existing task as REMOVED.  Raise KeyError if not found.'
        entry = self.entry_finder.pop(task)
        entry[-1] = self.REMOVED

    def pop_task(self):
        #'Remove and return the lowest priority task. Raise KeyError if empty.'
        while self.pq:
            priority, g, count, task = heappop(self.pq)
            if task is not self.REMOVED:
                del self.entry_finder[task]
                return priority, g, task
                raise KeyError('pop from an empty priority queue')


