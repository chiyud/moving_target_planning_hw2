# -*- coding: utf-8 -*-
"""
Created on Fri Oct 14 15:21:35 2016

HW2, a) Catch moving target

@author: chiyud
"""


from aux import *
import heapq as hq
import time
import os
import sys



#os.system("beep -f 555 -l 460")

if len(sys.argv) < 2:
    print('[Error] Please identify problem file.')
    print('$ python3 moving-target-a.py problem_<*>')
    exit()

problem = sys.argv[1]
print(problem)
#'problems/problem_1.txt'
path = 'problems/'+problem+'.txt'

tic = time.time()
C = loaddata(path)
toc = time.time()
print('Loaded data in ',toc-tic,' seconds')

PQ = priorityQ()

goals = {}

closelist = {}

for g in C.config['target']:
    # transfer to string 
    goals[toString(g)] = True


for key in goals:
    cost = C.getCost(toList(key))
    PQ.add_task(key,c = cost)

# Dijkstra generating heuristic
print('Start generating heuristic')
tic = time.time()

while(len(PQ.pq)>0):
    pri, g, key = PQ.pop_task()
    pos = toList(key)
    x = pos[1]
    y = pos[0]
    closelist[key] = g - C.getCost(pos)   
    # move left:
    
    new_pose = C.checkBoundary([y,x-1])
    if new_pose:
        strNewPose = toString(new_pose)
        if strNewPose not in closelist:
            PQ.add_task(strNewPose, g, C.getCost(new_pose))
            
                
    # move right:
    new_pose = C.checkBoundary([y,x+1])
    if new_pose:
         strNewPose = toString(new_pose)
         if strNewPose not in closelist:
            PQ.add_task(strNewPose, g, C.getCost(new_pose))
                               
    # move up:
    new_pose = C.checkBoundary([y-1,x])
    if new_pose:
        strNewPose = toString(new_pose)
        if strNewPose not in closelist:
            PQ.add_task(strNewPose, g, C.getCost(new_pose))
                               
    # move down:
    new_pose = C.checkBoundary([y+1,x])
    if new_pose:
        strNewPose = toString(new_pose)
        if strNewPose not in closelist:
            PQ.add_task(strNewPose, g, C.getCost(new_pose))


H = closelist

'''
with open('H_problem_1.pickle','rb') as f:
    H = pickle.load(f)
'''

toc = time.time()

print('Finish generating heuristic in ' , toc-tic,' seconds')

    # don't move:
    #new_pose = C.checkBoundary([y,x])
    #if new_pose:
    #    q2.append(node(pose = new_pose, \
    #                   cost_in_cell = \
    #                           (C.config['costs'])[new_pose[0]][new_pose[1]],\
    #                    parent = Node))                    



############
#
#  Generate configuration space
#
############
T = 0
goals = {}
PQ = priorityQ()

for g in C.config['target']:
    # transfer to string 
    g.append(T)
    goals[toString(g)] = -1;
    T += 1

print("Start A* search")
tic = time.time()
Init_pose = C.config['initial'][0]
Init_poseNT = toString(Init_pose)
Init_cost = C.getCost(Init_pose)
Init_pose.append(0)
weight =1.0
PQ.add_task(toString(Init_pose),0,0,weight*H[Init_poseNT])

reached_goal_counter = 0;
loopCounter = 0
A_closelist = {}
tic = time.time()
parent= {}

while(len(PQ.pq)>0 and reached_goal_counter < T):
    priority,g, key = PQ.pop_task()
    loopCounter +=1
    pos = toList(key)
    x = pos[1]
    y = pos[0]
    t = pos[2]
    A_closelist[key] = g 
    if key in goals:
        goals[key] = g
        reached_goal_counter +=1
        #os.system("beep -f 442 -l 460")
        print([len(A_closelist), reached_goal_counter, g, t])    
        break

    
    
    
    # move left:    
    new_pose = C.checkBoundary([y,x-1])
    if new_pose and t+1 <= T:
        strNewPoseNT = toString(new_pose)
        new_pose.append(t+1)
        strNewPose = toString(new_pose)
        if strNewPose not in A_closelist:
            if PQ.add_task(strNewPose, g, C.getCost([y,x-1]), weight*H[strNewPoseNT]):
                parent[strNewPose] = key
            
    # move right:
    
    new_pose = C.checkBoundary([y,x+1])
    if new_pose and t+1 <= T:
        strNewPoseNT = toString(new_pose)
        new_pose.append(t+1)
        strNewPose = toString(new_pose)
        if strNewPose not in A_closelist:
             if PQ.add_task(strNewPose, g, C.getCost([y,x+1]), weight*H[strNewPoseNT]):
                parent[strNewPose] = key
                 
                               
    # move up:
    new_pose = C.checkBoundary([y-1,x])
    if new_pose and t+1 <= T:
        strNewPoseNT = toString(new_pose)
        new_pose.append(t+1)
        strNewPose = toString(new_pose)
        if strNewPose not in A_closelist:
            if PQ.add_task(strNewPose, g, C.getCost([y-1,x]), weight*H[strNewPoseNT]):
                parent[strNewPose] = key
                
                               
    # move down:
    new_pose = C.checkBoundary([y+1,x])
    if new_pose and t+1 <= T:
        strNewPoseNT = toString(new_pose)
        new_pose.append(t+1)
        strNewPose = toString(new_pose)
        if strNewPose not in A_closelist:
           if PQ.add_task(strNewPose, g, C.getCost([y+1,x]), weight*H[strNewPoseNT]):
               parent[strNewPose] = key



    new_pose = C.checkBoundary([y,x])
    if new_pose and t+1 <= T:
        strNewPoseNT = toString(new_pose)
        new_pose.append(t+1)
        strNewPose = toString(new_pose)
        if strNewPose not in A_closelist:
           if PQ.add_task(strNewPose, g, C.getCost([y,x]), weight*H[strNewPoseNT]):
                parent[strNewPose] = key
              
    
    toc = time.time()


toc = time.time()
print("Finish A* searh in ", toc-tic, " seconds")

K = key
rst = []
rst.append(toList(key)[0:2])
while K != toString(Init_pose):
    curK = parent[K]
    rst.insert(0,toList(curK)[0:2])
    K = curK

print("Cost = ", g )
with open('rst_a_'+problem+'.txt','w') as f:
    f.write(str(g)+'\n')
    for i in rst:
        print(i[0],i[1])
        f.write(str(i[0])+','+str(i[1])+'\n')
        
#from visualization import rstShow
#rstShow(np.array(C.config['costs']), np.array(rst))    
