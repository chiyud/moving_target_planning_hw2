# Moving Target problem
	> This program was implemented and tested in **Python 3.5**
![Alt](/report/a_0.png "Title")
![Alt](/report/b_1.png "Title")
## For incoming new test:
Please save problem file into problems/ folder
Just type in problem file's name, the program will automatically search for it in problems/ folder.

## Usage:
```bash

$ python3 moving-target-a.py problem_0

$ python3 moving-target-a.py problem_1

$ python3 moving-target-b.py problem_0

$ python3 moving-target-b.py problem_1

```

## source files:
- moving-target-a.py
:	A* Search with admissible heuristic (uncomment last two lines for visualization)

- moving-target-b.py
:	Weighted A*, w = 1.01 for problem_1, w = 2 for problem_0. (uncomment last two lines for visualization)

- aux.py
:	Helper functions

- visualization.py
:	Plot world and path (no need to modify), disabled in default.

- rst_*_problem_*.txt
:	Results of different problems using different algorithms.

- problems/*.txt
:	Problem definiation files

- report/
:	Report and figures of this assignment

## Detailed implementation and discussions in report





