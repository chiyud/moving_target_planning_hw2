# -*- coding: utf-8 -*-
"""
Created on Sat Oct 22 11:34:11 2016

@author: chiyud
"""

# visualization

import matplotlib.pyplot as plt
import pickle
import aux
import numpy as np

def rstShow(costMap, rst):
    
    plt.matshow(costMap,cmap=plt.cm.gray, origin = 'upper')
    plt.xlim([-0.5,len(costMap[0])])
    plt.ylim([len(costMap),-0.5])
    plt.plot(rst[:,1],rst[:,0],'r')
    plt.show()
    
    print(rst)
    pass

'''
with open('solution_problem_1.pickle','rb') as f:
    C = pickle.load(f)
    rst = pickle.load(f)
    parent = pickle.load(f)
    
rstShow(np.array(C.config['costs']), np.array(rst))    
'''